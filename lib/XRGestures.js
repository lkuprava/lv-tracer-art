
////////////////////////////////////////////////////////////////////////////////// 
///   Nuclear Engineering Center at Georgian Technical University
///   TRACER AR Table aplikacia https://tracer-art.web.cern.ch
///////////////////////////////////////////////////////   Creation History
///   Name:     XRGestures.js
///   Subsystems: ART
///   Path: \tracer-art\lib\XRGestures.js
///   Description: AR controlis gamaaqtiurebeli biblioteka

///   Author: Luka Todua                
///   Date: 20.02.2021
///////////////////////////////////////////////////////   Change History
///   Name: Luka Todua        
///   Date:  03.06.2022      
///   Description: damemata komentarebi
//////////////////////////////////////////////////////////////////////////////////

//threejs is bibliotekis agwera
import * as THREE from './three.module.js';

//AR controlis klass chirdeba EventDispatcher threejs-is eventlisterenebis asamushaveblad
class XRGestures extends THREE.EventDispatcher{
    constructor( renderer ){
        super();
        
        //vamowmebt AR kontrolis gaaqtiurebisas mienicha tu ara renderer paramentri
        if (renderer === undefined){
            console.error('XRGestures must be passed a renderer');
            return;
        }
        
        const clock = new THREE.Clock();
        
        //AR-s scenas kontrolebis asamushaveblad esachiroeba AR shi arsebuli kontroleris funqciis gaaqtiureba
        //vxwert kontroler 1-s romelsac vanichebt funqcionals selectstart. selectend
        this.controller1 = renderer.xr.getController(0);
        this.controller1.userData.gestures = { index: 0 };
        this.controller1.userData.selectPressed = false;
        this.controller1.addEventListener( 'selectstart', onSelectStart );
        this.controller1.addEventListener( 'selectend', onSelectEnd );

        //vxwert kontroler 2-s romelsac vanichebt funqcionals selectstart. selectend
        this.controller2 = renderer.xr.getController(1);
        this.controller2.userData.gestures = { index: 1 };
        this.controller2.userData.selectPressed = false;
        this.controller2.addEventListener( 'selectstart', onSelectStart );
        this.controller2.addEventListener( 'selectend', onSelectEnd );
        
        //titis ormagi dacheris dro da limiti
        this.doubleClickLimit = 0.2;
        //titis didxans dacheris drois limiti 
        this.pressMinimum = 0.4;
        //titis marjvniv gasmis vectoris gansazgvra
        this.right = new THREE.Vector3(1,0,0);
        //titi zemot asmis veqtris gansazgvra
        this.up = new THREE.Vector3(0,1,0);
        //kontrolis tipis agwera da ucnobad gamocxadeba
        this.type = 'unknown';
        //titis amtvlelis agwera da 0-is minicheba
        this.touchCount = 0;
        
        this.clock = clock;
        
        const self = this;
        

        function onSelectStart( ){
            const data = this.userData.gestures;
            //sawyisi poziciis gansazgvra da misi amoucnobad gamocxadeba
            data.startPosition = undefined;
            //titis dacheris, anu scenis gaaaqtiurebis drois damaxsovreba
            data.startTime = clock.getElapsedTime();
            //ekranze titis dacheris tvla 
            if ( self.type.indexOf('tap') == -1) data.taps = 0;
            //tu ekranze moxda titis dachera
            self.type = 'unknown';
            //dafiqsirdes rogorc dacherili
            this.userData.selectPressed = true;
            //da counters daematos 1
            self.touchCount++;
            
            console.log( `onSelectStart touchCount: ${ self.touchCount }` );
        }
        
        function onSelectEnd( ){
            const data = this.userData.gestures;
            //titis agebis drois damaxsovreba
            data.endTime = clock.getElapsedTime();
            //sawyisi da sabooloo drois gamotvla
            const startToEnd = data.endTime - data.startTime;
            
            //console.log(`XRGestures.onSelectEnd: startToEnd:${startToEnd.toFixed(2)} taps:${data.taps}`);

            //tu moxda titis gasmis funqciis gaatiureba, ganisazgvros tu ra mimartulebit moxda es gasma da gaaaqtiurdes funqcionali
            if (self.type === 'swipe'){
                const direction = ( self.controller1.position.y < data.startPosition.y) ? "DOWN" : "UP";
                self.dispatchEvent( { type:'swipe', direction } );
                self.type = 'unknown';
            //tu agmochnda sxva tipis kontrolebi rogorciaa picnch anu zoomi, rotate modzraoba Rerzis garSemo, pan modzraoba sivrceSi
            //vamowmebt dawyeba dasrulebis formulis migebuli Sedegi naklebia tu ara ormagi titis daceris dros migebuli droze tu ki es chaitvleba rogorc
            //multiple dachera da gaaqtiurebs funqcias
            }else if (self.type !== "pinch" && self.type !== "rotate" && self.type !== 'pan'){
                if ( startToEnd < self.doubleClickLimit ){
                    self.type = "tap";
                    data.taps++;

                }else if ( startToEnd > self.pressMinimum ){
                    self.dispatchEvent( { type: 'press', position: self.controller1.position, matrixWorld: self.controller1.matrixWorld }   );
                    self.type = 'unknown';
                }
            }else{
                self.type = 'unknown';
            }
            
            //tu shesabamisi shedegebi ver miigo kontrols wyvets qmedebas
            this.userData.selectPressed = false;
            data.startPosition = undefined;
            
            //akldeba titis dacheris mtvlels erTeuli
            self.touchCount--;
        }
    }
    
    //multi dacheris funqcia
    get multiTouch(){
        let result;
        //tu controler 1 an 2 ar aris agwerili dagvibrunos uarjofiti Sedegi
        if ( this.controller1 === undefined || this.controller2 === undefined ){   
            result = false;
        }else{
            //tu arada dagvibrunos priveli da meore kontrolerebi ertad
            result = this.controller1.userData.selectPressed && this.controller2.userData.selectPressed;
        }
        const self = this;
        console.log( `XRGestures multiTouch: ${result} touchCount:${self.touchCount}`);
        return result;
    }
    
    //dacheris funqcia
    get touch(){
        let result;
        //tu controler 1 an 2 ar aris agwerili dagvibrunos uarjofiti Sedegi
        if ( this.controller1 === undefined || this.controller2 === undefined ){   
            result = false;
        }else{
            //tu romelime kontroller agwerilia da ekranze moxda titis dachera daabunos nebismieri erti kontroleri
            result = this.controller1.userData.selectPressed || this.controller2.userData.selectPressed;
        }
        //console.log( `XRGestures touch: ${result}`);
        return result;
    }
    
    get debugMsg(){
        return this.type;
    }
    
    update(){
        const data1 = this.controller1.userData.gestures;
        const data2 = this.controller2.userData.gestures;
        const currentTime = this.clock.getElapsedTime();
        
        let elapsedTime;
        
        if (this.controller1.userData.selectPressed && data1.startPosition === undefined){
            elapsedTime = currentTime - data1.startTime;
            if (elapsedTime > 0.05 ) data1.startPosition = this.controller1.position.clone();
        }
        
        if (this.controller2.userData.selectPressed && data2.startPosition === undefined){
            elapsedTime = currentTime - data2.startTime;
            if (elapsedTime > 0.05 ) data2.startPosition = this.controller2.position.clone();
        }
        //titis dacheris funqcionalis shemowmeba switch funqciastan ertad sadac titoeul dachieril titi agiqmeba rogorc controlleri romelsac
        //enichebat tipi, position da scenis matrixWorld paramentrebi
        if (!this.controller1.userData.selectPressed && this.type === 'tap' ){
            //Only dispatch event after double click limit is passed
            elapsedTime = this.clock.getElapsedTime() - data1.endTime;
            if (elapsedTime > this.doubleClickLimit){
                //console.log( `XRGestures.update dispatchEvent taps:${data1.taps}` );
                switch( data1.taps ){
                    case 1:
                        this.dispatchEvent( { type: 'tap', position: this.controller1.position, matrixWorld: this.controller1.matrixWorld } );
                        break;
                    case 2:
                        this.dispatchEvent( { type: 'doubletap', position: this.controller1.position, matrixWorld: this.controller1.matrixWorld } );
                        break;
                    case 3:
                        this.dispatchEvent( { type: 'tripletap', position: this.controller1.position, matrixWorld: this.controller1.matrixWorld } );
                        break;
                    case 4:
                        this.dispatchEvent( { type: 'quadtap', position: this.controller1.position, matrixWorld: this.controller1.matrixWorld }  );
                        break;
                }
                this.type = "unknown";
                data1.taps = 0;
            }
        }
        //shexevis funcionalis shemowmeba da ganawileba
        if (this.type === 'unknown' && this.touch){
            if (data1.startPosition !== undefined){
                if (this.multiTouch){
                    if (data2.startPosition !== undefined){
                        //startPosition is undefined for 1/20 sec
                        //test for pinch or rotate
                        //vitvlit sawyiss wertils threejs is distanceTo funqciit
                        const startDistance = data1.startPosition.distanceTo( data2.startPosition );
                        //vitvlit mimdinare mandzils kontrollerebs shoris
                        const currentDistance = this.controller1.position.distanceTo( this.controller2.position );
                        const delta = currentDistance - startDistance;
                        //tu mimdinare madzinlisa da sawyisi madzilis sxvaoba mitia 0.001 es chaitvleba zoom modzraobad da gaaqtiurebs funqcias
                        if ( Math.abs(delta) > 0.01 ){
                            this.type = 'pinch';
                            this.startDistance = this.controller1.position.distanceTo( this.controller2.position );
                            this.dispatchEvent( { type: 'pinch', delta: 0, scale: 1, initialise: true } );
                        //tu ara igi chaitvleba modzraobis fuqciad da gaaqtiurdeba rotate event
                        }else{
                            const v1 = data2.startPosition.clone().sub( data1.startPosition ).normalize();
                            const v2 = this.controller2.position.clone().sub( this.controller1.position ).normalize();
                            const theta = v1.angleTo( v2 );
                            if (Math.abs(theta) > 0.2){
                                this.type = 'rotate';
                                this.startVector = v2.clone();
                                this.dispatchEvent( { type: 'rotate', theta: 0, initialise: true } );
                            }
                        }
                    }
                }else{
                    
                    //modzraobis da titis asmis funqcionalis shemowmeba.
                    //titis asmis kontrolis shemowmebistvis vitvlit distancias da titis modzraobis dros da mis sichqares 
                    let dist = data1.startPosition.distanceTo( this.controller1.position );
                    elapsedTime = this.clock.getElapsedTime() - data1.startTime;
                    const velocity = dist/elapsedTime;
                    //console.log(`dist:${dist.toFixed(3)} velocity:${velocity.toFixed(3)}`);

                    //tu distancia metia 0.01 da sicqare metia 0.1-ze mashi fiqsirdeba rom es aris titis asmis funqica da aqtiurdeba event
                    if ( dist > 0.01 && velocity > 0.1 ){
                        const v = this.controller1.position.clone().sub( data1.startPosition );
                        let maxY = (Math.abs(v.y) > Math.abs(v.x)) && (Math.abs(v.y) > Math.abs(v.z));
                        if ( maxY )this.type = "swipe";
                    //tu arada  distancia 0.006 ze metia da sicqare naklebia 0.03-ze es itvleba ukve titis dacherad da dawyeba modzraobis event
                    }else if (dist > 0.006 && velocity < 0.03){
                        this.type = "pan";
                        this.startPosition = this.controller1.position.clone();
                        this.dispatchEvent( { type: 'pan', delta: new THREE.Vector3(), initialise: true } );
                    }
                }
            }
        //tu dafiqsirda zoomis event zoomis eventistvis kalkulacia xdeba tavidan 
        }else if (this.type === 'pinch'){
            const currentDistance = this.controller1.position.distanceTo( this.controller2.position );
            const delta = currentDistance - this.startDistance;
            const scale = currentDistance/this.startDistance;
            this.dispatchEvent( { type: 'pinch', delta, scale });
        //tu dafiqsirda rotate event kaklukacia xdeba tavidan
        }else if (this.type === 'rotate'){
            const v = this.controller2.position.clone().sub( this.controller1.position ).normalize();
            let theta = this.startVector.angleTo( v );
            const cross = this.startVector.clone().cross( v );
            if (this.up.dot(cross) > 0) theta = -theta;
            this.dispatchEvent( { type: 'rotate', theta } );
        }else if (this.type === 'pan'){
            const delta = this.controller1.position.clone().sub( this.startPosition );
            this.dispatchEvent( { type: 'pan', delta } );
        }
    }
}
//es faili sheqmnilia rogorc moduli aitom klasis sxva js failshi gamosayeneblad unda moxdes klasis export funqciit gamodzaxeba 
export { XRGestures };